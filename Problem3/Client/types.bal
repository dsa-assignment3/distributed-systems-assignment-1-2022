public type StudentCourses record {
    string courseCode?;
    string weight;
    string Marks;
};

public type InlineResponse400 record {
    string message?;
};

public type Student record {
    string studentNumber?;
    string name?;
    string email?;
    StudentCourses[] Courses?;
};
