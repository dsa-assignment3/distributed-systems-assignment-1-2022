import ballerina/http;
import ballerina/url;
import ballerina/lang.'string;
import ballerina/log;

public type StudentsListArr Student[];
public isolated client class Client {
    final http:Client clientEp;
    # Gets invoked to initialize the `connector`.
    #
    # + clientConfig - The configurations to be used when initializing the `connector` 
    # + serviceUrl - URL of the target service 
    # + return - An error if connector initialization failed 
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:8080/vle/api/v1") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
        return;
    }
    # Create Student Profile
    #
    # + return - New Student Profile created 
    remote isolated function createUser(Student[] payload) returns http:Response|error {
        string resourcePath = string `/students/create`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        http:Response response = check self.clientEp->post(resourcePath, request);
        return response;
    }
    # updates a Students profile
    #
    # + return - successfully updated learner profile 
    remote isolated function updateUser(string studentNumber, Student payload) returns http:Response|error {
        string resourcePath = string `/students/update`;
        map<anydata> queryParam = {"studentNumber": studentNumber};
        resourcePath = resourcePath + check getPathForQueryParam(queryParam);
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        http:Response response = check self.clientEp->put(resourcePath, request);
        return response;
    }
    # update student courses details
    #
    # + return - successfully updated student course details 
    remote isolated function updateStudentCourse(string studentNumber, string courseCode, Student payload) returns http:Response|error {
        string resourcePath = string `/students/updateStudentCourse`;
        map<anydata> queryParam = {"studentNumber": studentNumber, "courseCode": courseCode};
        resourcePath = resourcePath + check getPathForQueryParam(queryParam);
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        http:Response response = check self.clientEp->put(resourcePath, request);
        return response;
    }
    # Return information of a Students
    #
    # + studentNumber - studentNumber of the user 
    # + return - successfully retrieved 
    remote isolated function getUserByStudentNumber(string studentNumber) returns http:Response|error {
        string resourcePath = string `/Students/${getEncodedUri(studentNumber)}`;
        http:Response response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Delete user by student number
    #
    # + studentNumber - studentNumber of the user 
    # + return - User was successfully deleted 
    remote isolated function deleteUser(string studentNumber) returns http:Response|error {
        string resourcePath = string `/Students/${getEncodedUri(studentNumber)}`;
        http:Response response = check self.clientEp->delete(resourcePath);
        return response;
    }
}

isolated function getPathForQueryParam(map<anydata> queryParam) returns string|error {
    string[] param = [];
    param[param.length()] = "?";
    foreach var [key, value] in queryParam.entries() {
        if value is () {
            _ = queryParam.remove(key);
        } else {
            if string:startsWith(key, "'") {
                param[param.length()] = string:substring(key, 1, key.length());
            } else {
                param[param.length()] = key;
            }
            param[param.length()] = "=";
            if value is string {
                string updateV = check url:encode(value, "UTF-8");
                param[param.length()] = updateV;
            } else {
                param[param.length()] = value.toString();
            }
            param[param.length()] = "&";
        }
    }
    _ = param.remove(param.length() - 1);
    if param.length() == 1 {
        _ = param.remove(0);
    }
    string restOfPath = string:'join("", ...param);
    return restOfPath;
}
