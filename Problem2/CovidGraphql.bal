import ballerina/graphql;


public type CovidStatistics record {|

    string date;
    readonly string region;
    decimal deaths;
    decimal confirmedCases;
    decimal recoveries;
    decimal testedCases;
|};

table<CovidStatistics> key(region) TableEntry = table[
{ date:"12/09/2021", region:"Oshana", deaths: 20 ,confirmedCases: 423 ,recoveries: 41 ,testedCases:230},
{ date:"12/09/2021", region:"Khomas", deaths: 12 ,confirmedCases: 400 ,recoveries: 67 ,testedCases: 1200},
{ date:"12/09/2021", region:"Kunene", deaths: 9 ,confirmedCases: 104 ,recoveries: 37 ,testedCases: 560},
{ date:"12/09/2021", region:"Omusati", deaths: 32 ,confirmedCases: 65 ,recoveries: 50 ,testedCases: 440}

];

public distinct service class Data{
    private final readonly & CovidStatistics State;

   function init(CovidStatistics State){
        self.State = State.cloneReadOnly();
    }

    resource  function get State () returns CovidStatistics{
        return self.State;    
    }
    
    resource function get Date() returns string {
        return self.State.date; 
    }

    resource function get Region() returns string {
        return self.State.region;
    }
    resource function get Deaths() returns decimal? {
        if self.State.deaths is decimal{
        return self.State.deaths;
        }
        return;
    }

    resource function get confirmedCases() returns decimal? {
        if self.State.confirmedCases is decimal{
        return self.State.confirmedCases;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.State.recoveries is decimal{
        return self.State.recoveries;
        }
        return;
    }

    resource function get testedCases() returns decimal? {
        if self.State.testedCases is decimal{
        return self.State.testedCases;
        }
        return;
    }
}

service /graphql on new graphql:Listener(9090){
    resource function get all() returns Data[] {
        CovidStatistics[] covidEntries = TableEntry.toArray().cloneReadOnly();
        return covidEntries.map(query => new Data(query));
    }

    remote function add(CovidStatistics query ) returns Data{
        TableEntry.add(query);
        return new Data(query);
    }

}