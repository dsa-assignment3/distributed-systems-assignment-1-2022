import ballerina/http;

listener http:Listener ep0 = new (8080, config = {host: "localhost"});
Student[] students = [];

type student record{|
    string studentNumber;
    string name;
    string email;
|};

type Course record{|
    int CourseCode;
    string Weight;
    int Mark;
|};

//Student Search function
function SearchStudent(string studentNumber) returns Student? {
    Student? FindStudent = ();
    foreach var student in students {
        if student.studentNumber == studentNumber {
            findStudent = student;
            break;
        }
    }
    return findStudent;
}

//Search Course function
function SearchCourse(string CourseCode) returns Course? {
    Course? findCourse =();
    foreach var course in courses {
        if course.CourseCode == CourseCode {
            findCourse = ();
            return course;
            break;
        }
    }
    return findCourse;
}



service /vle/api/v1 on ep0 {
    //Create Student profile
    resource function post students/create(@http:Payload Student[] payload) returns http:Ok|BadRequestInlineResponse400 {
        Student.push(payload);
        return <http:Ok>{body:"Successfully created new student"};
    }
    
//Update student Details
    resource function put students/update(string studentNumber, @http:Payload Student payload) returns http:NotFound {
            if !(payload?.studentNumber is ()) 
            && !(payload?.name is ()) 
            && !(payload?.email is ()) {
                Student? Student = findStudent(studentNumber);
                if !(findStudent is ()) {
                    findStudent.studentNumber = payload?.studentNumber ?: "";
                    findStudent.name = payload?.name ?: "";
                    findStudent.email = payload?.email;
                } else {
                    return <http:NotFound>{body: "Failed to find student with studentNumber to update '" + studentNumber + "'"};
                }
            }
            return <http:Ok>{}
    }
    //Update student Course
    resource function put students/updateStudentCourse(string studentNumber, string courseCode, @http:Payload Student payload) returns http:Ok|BadRequestInlineResponse400 {
            if !(payload?.CoursesCode is ())
            && !(payload?.Weight is ()) 
            && !(payload?.Mark is ()){
                if!(Course is ()){
                findCourse.CourseCode = payload.CoursesCode;
                findCourse.Weight = payload.Weight;
                findCourse.Mark = payload.Mark;
                }else {
                    return <http:NotFound>body: "Failed to find student with course code to update '" + CourseCode + "'"};}
                }
            }
            return
    }
    //search for studnet using their username
    resource function get Students/[string  studentNumber]() returns Student[] {
    
    }
    //Return all students
    resource function get allStudent/[string  studentNumber]() returns Student[] {
        return allstudents;
    }
    //Delete a student profile 
    resource function delete Students/[string  studentNumber]() returns http:NoContent|BadRequestInlineResponse400 {
    }
}
